import os
import json
from shutil import copyfile

configs = json.loads(open("configs.json", "r").read())
code_base_directory = configs["code_base_directory"]

print("Generating ks files...")

os.system('cd {}PythonServer && python generate_ks.py'.format(code_base_directory))

os.remove('{}PythonClient\\ks\\commands.ks'.format(code_base_directory))
os.remove('{}PythonClient\\ks\\commands.py'.format(code_base_directory))
os.remove('{}PythonClient\\ks\\models.ks'.format(code_base_directory))
os.remove('{}PythonClient\\ks\\models.py'.format(code_base_directory))

os.remove('{}PythonRandomClient\\ks\\models.ks'.format(code_base_directory))
os.remove('{}PythonRandomClient\\ks\\models.py'.format(code_base_directory))
os.remove('{}PythonRandomClient\\ks\\commands.ks'.format(code_base_directory))
os.remove('{}PythonRandomClient\\ks\\commands.py'.format(code_base_directory))

copyfile('{}PythonServer\\ks\\commands.ks'.format(code_base_directory), '{}PythonClient\\ks'.format(code_base_directory))
copyfile('{}PythonServer\\ks\\commands.py'.format(code_base_directory), '{}PythonClient\\ks'.format(code_base_directory))
copyfile('{}PythonServer\\ks\\models.ks'.format(code_base_directory), '{}PythonClient\\ks'.format(code_base_directory))
copyfile('{}PythonServer\\ks\\models.py'.format(code_base_directory), '{}PythonClient\\ks'.format(code_base_directory))

copyfile('{}PythonServer\\ks\\commands.ks'.format(code_base_directory), '{}PythonRandomClient\\ks'.format(code_base_directory))
copyfile('{}PythonServer\\ks\\commands.py'.format(code_base_directory), '{}PythonRandomClient\\ks'.format(code_base_directory))
copyfile('{}PythonServer\\ks\\models.ks'.format(code_base_directory), '{}PythonRandomClient\\ks'.format(code_base_directory))
copyfile('{}PythonServer\\ks\\models.py'.format(code_base_directory), '{}PythonRandomClient\\ks'.format(code_base_directory))


print("Done.")
