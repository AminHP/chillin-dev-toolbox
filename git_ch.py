import os
import sys

command = sys.argv
apply_command = True

if "gti" in command[1] or "dev" in command[1] or "master" in command[1]:

    print("SUSPICIOUS CODE! ARE YOU SURE YOU WANT TO RUN THIS COMMAND?")
    answer = input("Enter Y/N: ")
    if answer == "N" or answer == "n":
        apply_command = False

if apply_command:
    os.system(command[1])
